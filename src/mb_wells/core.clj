(ns mb-wells.core
  (:require [clj-http.client :as client]
            [clojure.string :as s]
            [clojure.data.csv :as csv]
            [clojure.data.json :as json]
            [cemerick.url :as url]
            [clojure.java.io :as io])
  )



(defn parse-json-resp [resp]
  (-> resp
      :body
      (json/read-str :key-fn keyword))
  )
(def districts [ "01", "02", "03", "04", "05", "06", "6E", "7B", "7C", "08", "8A", "09", "10"])

(defn get-well-csv [district]
  (let [[cols & rest ] (->> (client/post "http://webapps2.rrc.texas.gov/EWA/orphanWellQueryAction.do"
                                         {:form-params {"actionManager.currentIndexHndlr.inputValue" "1",
                                                        "actionManager.actionRcrd%5B1%5D.actionParameterHndlr.inputValue"
                                                        "methodToCall",
                                                        "actionManager.actionRcrd%5B1%5D.pagerParameterKeyHndlr.inputValue"
                                                        "pager.paramValue",
                                                        "actionManager.actionRcrd%5B1%5D.hostHndlr.inputValue"
                                                        "webapps2.rrc.texas.gov%3A80",
                                                        "actionManager.actionRcrd%5B0%5D.actionHndlr.inputValue"
                                                        "%2ForphanWellQueryAction.do",
                                                        "actionManager.actionRcrd%5B1%5D.argRcrdParameters%28searchArgs.paramValue%29"
                                                        "%7C102%3D01",
                                                        "searchArgs.districtCodeArgHndlr.inputValue" "01",
                                                        "actionManager.actionRcrd%5B0%5D.hostHndlr.inputValue"
                                                        "webapps2.rrc.texas.gov%3A80",
                                                        "pager.pageSize" "10",
                                                        "actionManager.actionRcrd%5B1%5D.argRcrdParameters%28pager.paramValue%29"
                                                        "%7C1%3D1%7C2%3D10%7C3%3D1053%7C4%3D0%7C5%3D106%7C6%3D10",
                                                        "actionManager.actionRcrd%5B1%5D.actionHndlr.inputValue"
                                                        "%2ForphanWellQueryAction.do",
                                                        "actionManager.actionRcrd%5B0%5D.contextPathHndlr.inputValue" "%2FEWA",
                                                        "methodToCall" "generateOrphanWellReportCsv",
                                                        "actionManager.actionRcrd%5B1%5D.contextPathHndlr.inputValue" "%2FEWA",
                                                        "actionManager.actionRcrd%5B0%5D.actionParameterHndlr.inputValue"
                                                        "methodToCall",
                                                        "actionManager.actionRcrd%5B0%5D.argRcrdParameters%28searchArgs.paramValue%29"
                                                        "%7C102%3D01",
                                                        "actionManager.recordCountHndlr.inputValue" "2"}}
                                         )
                            :body
                            csv/read-csv
                            (drop-while (fn [row]
                                          (<= (count row) 1)
                                          ))
                            )]
    (println (count rest))
    (->> rest
         (map (fn [row]
                (zipmap cols row))))))

(defn read-wells* [csv-path]
  (let [[keys* & rest] (-> (slurp csv-path)
                          csv/read-csv
                          )
        keys (->> keys* (map (fn [k] (-> (s/lower-case k)
                                         (s/replace #"[ _]" "-")
                                         keyword
                                         ))))]
    (->> rest
         (map (fn [row]
                (zipmap keys row))))))


(def read-wells (memoize read-wells*))

(defn get-well-locations [apis]
  (println "QUERY")
  (let [query (->> apis
                   (map (fn [api]
                          (format "API = '%s'" api)
                          ))
                   (s/join " OR "))]
    (->> (client/get "https://gis.rrc.texas.gov/server/rest/services/rrc_public/RRC_Public_Viewer_Srvs/MapServer/1/query"
                     {:query-params {"f" "json",
                                     "where" query
                                     "outFields" "*"
                                     "returnGeometry" "false"}}
                     )
         parse-json-resp
         :features
         (map (fn [{{:keys [GIS_LAT83 GIS_LONG3 API]} :attributes}]
                {:lat GIS_LAT83
                 :lon GIS_LAT83
                 :api API}
                )))))

(defn get-bulk-well-locations [wells]
  (->> wells
       (partition-all 75)

       (mapcat (fn [wells-chunk]
                 (let [apis (->> wells-chunk (map :api))
                       coords (get-well-locations apis)]

                   (->> (concat wells-chunk coords)
                        (group-by :api)
                        (map (fn [[api datas]]
                               (apply merge datas)
                               ))
                        )
                   )
                 ))
       )
  )

(defn write-csv [filename data & [cols]]
  (let [cols (or cols
                 (->> data
                      (reduce (fn [col m] (into col (keys m))) #{})
                      (into [])))
        pretty-col-names (->> cols (map (comp s/capitalize name)))
        rows (->> data
                  (map #(dissoc % :description))
                  (map (apply juxt cols)))]
    (io/make-parents filename)
    (with-open [writer (io/writer filename)]
      (csv/write-csv writer (concat [pretty-col-names] rows)))))




(def query
  "f=json&returnGeometry=true&spatialRel=esriSpatialRelIntersects&geometry=%7B%22xmin%22%3A-11002040.103014652%2C%22ymin%22%3A3374078.2991627473%2C%22xmax%22%3A-10999594.11810946%2C%22ymax%22%3A3376524.284067938%2C%22spatialReference%22%3A%7B%22wkid%22%3A102100%7D%7D&geometryType=esriGeometryEnvelope&inSR=102100&outFields=*&outSR=102100")


(->> (s/split query #"&")
     (map (fn [v]
            (s/split v #"=")
            ))
     (filter (fn [[ k v]]
               v
               ))
     (into {})
     )
